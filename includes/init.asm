init
	lda #12
	sta $d020
	
	lda #15
	sta $d021

	lda #147
	jsr $ffd2

	sei

	lda #$7f
	sta $dc0d
	sta $dd0d

	lda $dc0d
	lda $dd0d

	lda #$01
	sta $d01a
	
	; Rasterline trigger setup
	lda #0
	sta $d012

	lda #$1b
	sta $d011

	; Turn KERNAL and BASIC off
	lda #$35
	sta $01
	
	; Set lowercase charset
	lda $d018
	ora #2
	sta $d018
	
	lda #<irq
	sta $fffe
	lda #>irq
	sta $ffff

	lda #0
	sta findPathProcess

	.if MANUAL_BRANCHES_RUN || MANUAL_PATHS_RUN || DRAW
		lda #0
		sta listScreenOffset

		jsr setWhiteForeground
		jsr copyRomCharset
	.fi

	cli
rts
