irq
	pha
	txa
	pha
	tya
	pha

	inc $d019
	
	.if RASTERLINE_INDICATION
		inc $d020
	.fi

	lda findPathProcess
	beq +
		inc framesCount
+
	
	; hit fire button to start, or press RETURN key
	jsr waitingForAction

	.if MANUAL_BRANCHES_RUN || MANUAL_PATHS_RUN || DRAW
		jsr waitingStepKeys
	.fi
	
	.if RASTERLINE_INDICATION
		dec $d020
	.fi
	
	pla
	tay
	pla
	tax
	pla
rti

triggerFindPathProcess
	lda findPathProcess
	bne +
		.if FRAMES_COUNT
			lda #0
			sta framesCount
		.fi
		lda #1
		sta findPathProcess
+
rts

; Wait for joystick port 2 fire button pressed, or RETURN key
waitingForAction
	; Looking for RETURN key pressed
	lda #%11111110
	sta $dc00
	lda $dc01
	and #%00000010
	bne +
		jsr triggerFindPathProcess
+

	; Looking for fire button hit
	lda #%00010000
	bit $dc00
	bne +
		jsr triggerFindPathProcess
+
rts

.if MANUAL_BRANCHES_RUN || MANUAL_PATHS_RUN || DRAW

waitingStepKeys

	; RETURN key release check
	lda #%11111110
	sta $dc00
	lda $dc01
	and #%00000010
	beq +
		; If RETURN key is not pressed, reset next step ctrl
		lda #0
		sta nextStepCtrl
+

	; Fire button (joystick port 2) release check
	lda #%00010000
	bit $dc00
	beq +
		; If fire button is not pressed, reset next step ctrl
		lda #0
		sta nextStepCtrlFireButton
+

	; SPACE bar release check
	lda #%01111111
	sta $dc00
	lda $dc01
	and #%00010000
	beq +
		; If key B is not pressed, reset toggle screen ctrl
		lda #0
		sta toggleScreenCtrl
+

	; Cursor keys UP / DOWN for scrolling on branches / paths list release check
	lda #%11111110
	sta $dc00
	lda $dc01
	and #%10000000
	beq +
		; If cursor keys UP / DOWN are not pressed, reset list screen scroll ctrl
		lda #0
		sta listScreenScrollCtrl
+
rts

.fi
