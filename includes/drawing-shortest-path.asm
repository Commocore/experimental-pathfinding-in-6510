
	plyX = $70
	plyY = $71
	plyDirection = $72
	plyTempDirection = $73
	plyFromDirection = $74

drawShortestPath
	lda xStart
	sta plyX
	lda yStart
	sta plyY
	
	lda mapPointer+1
	sta word
	lda mapPointer+2
	sta word+1

	ldy #0
	sty currentStep
-
	; Start to go in the direction from node's starting point
	ldy currentStep
	lda outputTrackDir,y ; get starting direction for node
	sta plyDirection

	tay
	lda fromDirectionTable,y
	sta plyFromDirection

	tya
	jsr followPathToNextNode

	; Compare with end node
	lda xEnd
	cmp plyX
	bne +
		lda yEnd
		cmp plyY
		bne +
			; End point reached, exit...
			rts
+
	
	inc currentStep
	jmp -

rts
	

; input: A - direction
followPathToNextNode
	sta plyTempDirection
	jmp followPathInDirection

followPathNextDirection
	lda plyTempDirection
-
	asl
	cmp #16
	bne +
		lda #1
+
	cmp plyFromDirection ; we cannot go back, as path always leads from exactly one point to exactly another one
	bne +
		jmp -
+
jmp followPathToNextNode


; input: A - direction
followPathInDirection
	cmp #1
	bne +
		; Check if we can access block to the north
		ldy plyY
		beq followPathNextDirection
			dey
			ldx plyX
			jsr getBlock
			beq followPathNextDirection
				dec plyY
				jmp followPathAdvance
+

	cmp #2
	bne +
		; Check if we can access block to the east
		ldx plyX
		cpx #MAX_X-1
		beq followPathNextDirection
			inx
			ldy plyY
			jsr getBlock
			beq followPathNextDirection
				inc plyX
				jmp followPathAdvance
+

	cmp #4
	bne +
		; Check if we can access block to the south
		ldy plyY
		cpy #MAX_Y-1
		beq followPathNextDirection
			iny
			ldx plyX
			jsr getBlock
			beq followPathNextDirection
				inc plyY
				jmp followPathAdvance
+

.if DEBUG_MODE

	; DEBUG: It shouldn't happen... never, as it should be the only last available option
	cmp #8
	beq +
		.byte 2 ; JAM!
		.text "debug#3" ; small trick to see this string in monitor, so it's easier to find this place in code
+

.fi

		; Check if we can access block to the west
		ldx plyX
		beq followPathNextDirection
			dex
			ldy plyY
			jsr getBlock
			beq followPathNextDirection
				dec plyX
jmp followPathAdvance


followPathAdvance
	; Mark path
	ldx plyX
	ldy plyY
	jsr drawFinalPathBlock

	; Check if we've reached the next node
	ldy currentStep
	lda outputTrackX,y
	cmp plyX
	bne +
		lda outputTrackY,y
		cmp plyY
		bne +
			; Node reached!
			rts
+

	; Try to follow in the direction which worked as the last one
	ldy plyTempDirection
	
	; Set the last direction from which we came, as we don't want go back
	lda fromDirectionTable,y
	sta plyFromDirection

	tya
	jmp followPathToNextNode


; input: X - x position
; input: Y - y position
drawFinalPathBlock
	stx xMem

	clc
	lda verticalColorRamTableLo,y
	adc xMem
	sta setColorRamFinalPathBlock+1
	lda verticalColorRamTableHi,y
	adc #0
	sta setColorRamFinalPathBlock+2

	cpx xEnd
	bne +
		cpy yEnd
		bne +
			lda #10
			.byte $2c
+
	lda #5
setColorRamFinalPathBlock
	sta $1234
rts
