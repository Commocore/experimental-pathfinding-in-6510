
.if MANUAL_BRANCHES_RUN

displayCurrentNodeMessage
	sty var1 ; preserve Y register

	ldy #0
	jsr setScreenLocation
	
	lda currentNode
	ldy #6
	jsr displayNodeValue

	lda #45
	sta FIRST_SCREEN + 55
	sta FIRST_SCREEN + 58

	jsr waitingStep

	ldy var1 ; restore Y register
rts


displayDirectionMessage
	lda direction
	tay
	lda directionText,y
	sta FIRST_SCREEN + 55
	
	lda direction
	clc
	adc #48
	sta FIRST_SCREEN + 58
	
	jsr waitingStep
rts


initBranchesDisplay
	ldx #size(nodeText)-1
-
	lda nodeText,x
	sta FIRST_SCREEN,x
	dex
	bpl -

	ldx #size(followBranchText)-1
-
	lda followBranchText,x
	sta FIRST_SCREEN + 40,x
	dex
	bpl -
rts

.fi


.if MANUAL_PATHS_RUN

displayPathStepStatusMessage
	sty var1 ; preserve Y register

	; Display step number
	ldy #0
	jsr setScreenLocation

	ldx #0
-
	lda stepText,x
	bne +
		lda currentStep
		jsr extract8bitNumber
		jsr displayNumber
		jmp ++
+
		sta (screen),y
		iny
+
	inx
	cpx #size(stepText)
	bne -

	; Display path id
	ldy #1
	jsr setScreenLocation

	ldx #0
	ldy #0
-
	lda pathText,x
	bne +
		lda currentPath
		jsr extract8bitNumber
		jsr displayNumber
		jmp ++
+
		sta (screen),y
		iny
+
	inx
	cpx #size(pathText)
	bne -
	
	; Display seek node id
	ldy #2
	jsr setScreenLocation

	ldx #0
	ldy #0
-
	lda seekNodeText,x
	bne +
		lda seekNode
		jsr displayNodeValue
		jmp ++
+
		sta (screen),y
		iny
+
	inx
	cpx #size(seekNodeText)
	bne -

	jsr waitingStep
	
	ldy var1 ; restore Y register
rts


displaySourceBranchMessage
	ldy #3
	jsr setScreenLocation

	ldy #0
-
	lda sourceBranchText,y
	sta (screen),y
	iny
	cpy #size(sourceBranchText)
	bne -
	
	lda yMem
	jsr extract8bitNumber
	jsr displayNumber

	jsr waitingStep
	jsr clearLastLine
rts


displayPathLoopMessage
	ldy #3
	jsr setScreenLocation
	
	ldy #0
	ldx #0
	stx var1
-
	lda pathLoopText,x
	bne ++
		inc var1
		lda var1
		cmp #1
		bne +
			lda currentPath
			jsr extract8bitNumber
			jsr displayNumber
			jmp +++
+
		lda aMem
		jsr extract8bitNumber
		jsr displayNumber
		jmp ++
+
	sta (screen),y
	iny
+
	inx
	cpx #size(pathLoopText)
	bne -
	
	
	jsr waitingStep
	jsr clearLastLine
rts


displayRegisterBranchToPathMessage
	lda #0
	sta var1

	ldy #3
	jsr setScreenLocation
	
	ldy #0
	ldx #0
-
	lda registerBranchToPathText,x
	bne ++
		inc var1
		lda var1
		cmp #1
		bne +
			lda aMem
			jsr extract8bitNumber
			jsr displayNumber
			jmp +++
+
			lda currentPath
			jsr extract8bitNumber
			jsr displayNumber
		jmp ++
+
	sta (screen),y
	iny
+
	inx
	cpx #size(registerBranchToPathText)
	bne -

	jsr waitingStep
	jsr clearLastLine
rts


displayMaxPathsOverflowMessage
	ldy #3
	jsr setScreenLocation
	
	ldy #0
-
	lda maxPathsOverflowText,y
	sta (screen),y
	iny
	cpy #size(maxPathsOverflowText)
	bne -
	
	jsr waitingStep
	jsr clearLastLine
rts


displayClonePathMessage
	lda #0
	sta var1
	
	ldy #3
	jsr setScreenLocation
	
	ldx #0
	ldy #0
-
	stx xMem
	lda clonePathText,x
	bne +++
		inc var1
		lda var1
		cmp #1
		bne +
			; Display current path
			lda currentPath
			jsr extract8bitNumber
			jsr displayNumber
			jmp ++++
+
		cmp #2
		bne +
			; Display source path id
			clc
			lda registeredPaths
			adc registeredPathsStack
			jsr extract8bitNumber
			jsr displayNumber
			jmp +++
+
			; Display destination path id
			lda aMem
			jsr extract8bitNumber
			jsr displayNumber
			jmp ++
+
		sta (screen),y
		iny
+
	ldx xMem
	inx
	cpx #size(clonePathText)
	bcc -
	
	jsr waitingStep
	jsr clearLastLine
rts


clearLastLine
	lda #32
	ldy #39
-
	sta FIRST_SCREEN + 40*3,y
	dey
	bpl -
rts


.fi


.if MANUAL_BRANCHES_RUN || MANUAL_PATHS_RUN || DRAW

; Wait for joystick port 2 fire button or RETURN key pressed, then wait for release
waitingStep

	jsr isFirstScreenVisible
	beq ++

		; Looking for pressing RETURN key on the first screen
		lda nextStepCtrl
		bne +
			lda #%11111110
			sta $dc00
			lda $dc01
			and #%00000010
			bne +
				inc nextStepCtrl
				rts
+

		; Looking for hit of fire button on the first screen
		lda nextStepCtrlFireButton
		bne +
			lda #%00010000
			bit $dc00
			bne +
				inc nextStepCtrlFireButton
				rts
+

	; Looking for pressing SPACE bar
	lda toggleScreenCtrl
	bne +
		lda #%01111111
		sta $dc00
		lda $dc01
		and #%00010000
		bne +
			inc toggleScreenCtrl
			jsr toggleScreens
+

	jsr isFirstScreenVisible
	bne +
		jsr lookingForSecondScreenScrollingKeys
+

jmp waitingStep


; Looking for pressing cursor key UP/DOWN on the second screen
lookingForSecondScreenScrollingKeys
	lda listScreenScrollCtrl
	beq +
		rts
+
	lda #%11111110
	sta $dc00
	lda $dc01
	and #%10000000
	bne ++++
		lda #1
		sta listScreenScrollCtrl

		; is RSHIFT key pressed? If yes, it's UP key
		lda #%10111111
		sta $dc00
		lda $dc01
		and #%00010000
		bne ++
			; Scroll up
			dec listScreenOffset
			bpl +
				lda #0
				sta listScreenOffset
				rts
+
			jsr renderSecondScreen
			rts
+
		; Scroll down
		inc listScreenOffset
		lda #230
		cmp listScreenOffset
		bcs +
			sta listScreenOffset
			rts
+
		jsr renderSecondScreen
+
rts


; output: 0 = false, 1 = true
isFirstScreenVisible
	lda $d018
	and #%11110000
	sta var4
	
	lda FIRST_SCREEN_MEMORY
	bit var4
rts


toggleScreens
	jsr isFirstScreenVisible
	beq +
		; Switch to the second screen
		jsr selectSecondScreen
		rts
+
	; Switch to the first screen
	jsr selectFirstScreen
	rts


setColorRamFirstScreen
	.for y=0, y<MAX_Y, y=y+1
		ldx #MAX_X-1
-
		lda COLOR_RAM_STORAGE + y*40 + MAP_SCREEN_OFFSET,x
		sta COLOR_RAM + y*40 + MAP_SCREEN_OFFSET,x
		dex
		bpl -
	.next
rts


setColorRamSecondScreen
	.for y=0, y<MAX_Y, y=y+1
		ldx #MAX_X-1
-
		lda COLOR_RAM + y*40 + MAP_SCREEN_OFFSET,x
		sta COLOR_RAM_STORAGE + y*40 + MAP_SCREEN_OFFSET,x
		lda #1
		sta COLOR_RAM + y*40 + MAP_SCREEN_OFFSET,x
		dex
		bpl -
	.next
rts

; Set the first screen with map and marked tracks
selectFirstScreen
	jsr setColorRamFirstScreen
	
	; Set charset
	lda $d018
	and #240
	ora FIRST_SCREEN_CHARSET
	sta $d018
	
	; Set screen memory
	lda $d018
	and #15
	ora FIRST_SCREEN_MEMORY
	sta $d018

	; Set memory bank #0
	lda $dd00
	and #252
	ora #3
	sta $dd00
rts


renderSecondScreen
	jsr cleanSecondScreen
	jsr listBranches
	jsr listPaths
rts


; Set the second screen with branches and paths list, but print live results first
selectSecondScreen
	jsr setColorRamSecondScreen
	jsr renderSecondScreen

	; Set charset
	lda $d018
	and #240
	ora SECOND_SCREEN_CHARSET
	sta $d018
	
	; Set screen memory
	lda $d018
	and #15
	ora SECOND_SCREEN_MEMORY
	sta $d018
	
	; Set memory bank #3
	lda $dd00
	and #252
	sta $dd00
rts


displayCannotFindExitMessage
	jsr cleanMessages

	ldy #0
	jsr setScreenLocation
	ldy #0
-
	lda cannotFindExitText,y
	sta (screen),y
	iny
	cpy #size(cannotFindExitText)
	bne -
	
	jsr waitingStep
	jsr cleanMessages
	jsr selectFirstScreen
	.if DRAW
		jsr drawMap
	.fi
rts


displayFinalMessage
	jsr cleanMessages

	; First line of final text
	ldy #0
	jsr setScreenLocation
	ldx #0
-
	lda finalText,x
	bne +
		lda shortestPath
		jsr extract8bitNumber
		jsr displayNumber
		jmp ++
+
	sta (screen),y
	iny
+
	inx
	cpx #size(finalText)
	bne -
	
	; Second line of final text
	ldy shortestPath
	lda pathsLengthTable,y
	sta var2
	ldy #1
	jsr setScreenLocation
	ldy #0
	ldx #0
	stx var1
-
	lda finalText2,x
	bne ++
		inc var1
		lda var1
		cmp #1
		bne +
			; Display shortest path id
			lda shortestPath
			jsr extract8bitNumber
			jsr displayNumber
			jmp +++
+
		; Display shortest path length
		lda var2
		jsr extract8bitNumber
		jsr displayNumber
		jmp ++
+
	sta (screen),y
	iny
+
	inx
	cpx #size(finalText2)
	bne -

	; Third line of final text
	ldy #2
	jsr setScreenLocation
	ldy #0
-
	lda finalText3,y
	sta (screen),y
	iny
	cpy #size(finalText3)
	bne -
rts


finalResultWait
	jsr waitingStep
	jsr cleanMessages
	jsr selectFirstScreen
rts


displayListMoreResultsMessage
	ldy #24
	jsr setSecondScreenLocation
	ldy #size(listMoreResultsText)-1
-
	lda listMoreResultsText,y
	sta (screen),y
	dey
	bpl -
rts


cleanMessages
	ldx #39
	lda #32
-
	sta FIRST_SCREEN,x
	sta FIRST_SCREEN + 40,x
	sta FIRST_SCREEN + 80,x
	sta FIRST_SCREEN + 120,x
	dex
	bpl -
rts


cleanSecondScreen
	ldy #0
	lda #32
-
	.for i=0, i<=3, i=i+1
		sta SECOND_SCREEN+250*i,y
	.next
	iny
	cpy #250
	bne -
rts


listBranches
	; Display header
	ldy #0
	sty var4 ; set row to display
	jsr setSecondScreenLocation
	ldy #size(listBranchesText)-1
-
	lda listBranchesText,y
	sta (screen),y
	dey
	bpl -

	lda registeredBranches
	bne +
		; Display no results yet if empty list
		ldy #1
		jsr setSecondScreenLocation

		ldy #size(listEmptyText)-1
-
		lda listEmptyText,y
		sta (screen),y
		dey
		bpl -
		rts
+

	lda listScreenOffset ; displayed branch offset
	beq +
		jsr displayListMoreResultsMessage
+

	ldx listScreenOffset
	inx ; branches are starting always from 1
	cpx registeredBranches
	beq +
	bcc +
		rts ; nothing to display for this screen offset
+
listBranchesLoop
	stx var3
	
	inc var4
	ldy var4
	jsr setSecondScreenLocation

	; Display branch id
	txa
	jsr extract8bitNumber
	ldy #0
	jsr displayNumber

	lda #58
	sta (screen),y
	iny
	lda #32
	sta (screen),y
	iny
	
	; Display first node
	sty var2
	ldy var3 ; get branch id
	lda branchesSourceNodeTable,y
	ldy var2
	jsr displayNodeValue
	
	; Display separator
	lda #45
	sta (screen),y
	iny
	
	; Display second node
	sty var2
	ldy var3 ; get branch id
	lda branchesDestinationNodeTable,y
	ldy var2
	jsr displayNodeValue
	
	; Display length of branch
	ldx #0
-
	lda (listBranchesLengthText),x
	sta (screen),y
	iny
	inx
	cpx #size(listBranchesLengthText)
	bcc -
	sty var2
	ldy var3 ; get branch id
	lda branchesLengthTable,y
	ldy var2
	jsr extract8bitNumber
	jsr displayNumber
	ldx var3

	lda var4
	cmp #23 ; maximum number of branches possible to display on the screen at once
	beq +
		inx
		cpx registeredBranches
		bcc listBranchesLoop
		beq listBranchesLoop
		rts
+

	lda listScreenOffset
	bne + ; if offset is already set, screen has been scrolled, so display list more results message
		cpx registeredBranches	; check if there is more branches available, if yes, display list more results message
		bcs +
		beq +
+
		jsr displayListMoreResultsMessage
+
rts


listPaths
	; Display header
	ldy #0
	sty var4 ; set row to display
	jsr setSecondScreenLocation
	ldx #0
	ldy #17
-
	lda listPathsText,x
	sta (screen),y
	iny
	inx
	cpx #size(listPathsText)
	bne -

	lda registeredPaths
	bne +
		; Display no results yet if empty list
		ldy #1
		jsr setSecondScreenLocation

		ldx #0
		ldy #17
-
		lda listEmptyText,x
		sta (screen),y
		iny
		inx
		cpx #size(listEmptyText)
		bne -
		rts
+

	clc
	lda registeredPaths
	adc registeredPathsStack
	sta var5

	cmp #24
	bcc +
		jsr displayListMoreResultsMessage
+

	ldx listScreenOffset ; displayed path offset
	cpx var5
	bcc +
		rts
+
listPathsLoop
	stx var2 ; set iterated path id

	; Display each path
	ldy var2 ; get path id
	lda pathsBranchPointerLoTable,y
	sta readPathToDisplay+1
	lda pathsBranchPointerHiTable,y
	sta readPathToDisplay+2

	inc var4
	ldy var4
	jsr setSecondScreenLocation
	
	; Display path id
	lda var2 ; path id
	jsr extract8bitNumber
	ldy #17 ; Set display starting position for each row
	jsr displayNumber

	; Display separator
	lda #58
	sta (screen),y
	iny
	
	; Display whitespace
	lda #32
	sta (screen),y
	iny
	sty var3

	; Display each branch of path
	ldx #0
	ldy var3
readPathToDisplay
	lda $1234,x
	beq ++
		jsr extract8bitNumber
		cpx #0
		beq +
			lda #45
			sta (screen),y
			iny
+
		jsr displayNumber

		inx
		jmp readPathToDisplay
+
	ldx var2

	lda var4
	cmp #23 ; maximum number of branches possible to display on the screen at once
	beq +
		inx
		cpx var5 ; compare with registeredPaths + registeredPathsStack
		bcc listPathsLoop
+
rts


displayNumber
	clc
	lda number100
	beq +
		adc #48
		sta (screen),y
		iny
+
	lda number10
	beq +
		adc #48
		sta (screen),y
		iny
+
	lda number1
	adc #48
	sta (screen),y
	iny
rts


displayNodeValue
	clc
	cmp #10
	bcc +
		adc #65-11
		jmp ++
+
	adc #48
+
	sta (screen),y
	iny
rts


;input: A register
;output: affects number_1, number_10 and number_100
extract8bitNumber

	sta number

	lda #0
	sta number100
	sta number10
	sta number1

	lda number

	; hundreds
	sec
-
		cmp #100
		bcc +
		sbc #100
		inc number100
		jmp -
+

	; tens
	sec
-
		cmp #10
		bcc +
		sbc #10
		inc number10
		jmp -
+

	; ones
	clc
	sta number1

rts


; input: Y - screen row
setScreenLocation
	lda screenLocationLo,y
	sta screen
	lda screenLocationHi,y
	sta screen+1
rts


; input: Y - screen row
setSecondScreenLocation
	lda secondScreenLocationLo,y
	sta screen
	lda secondScreenLocationHi,y
	sta screen+1
rts


setWhiteForeground
	; Set Color RAM colour
	ldy #0
	lda #1
-
	.for i=0, i<=3, i=i+1
		sta COLOR_RAM+250*i,y
	.next
	iny
	cpy #250
	bne -
rts


; Copy standard ROM charset to memory bank #3
copyRomCharset
	lda $01
	sta aMem

	; Enable Character ROM visible at $D000 - $DFFF
	lda $01
	and #%11111011 ; Set Character ROM visible
	ora #1 ; but we need to set bit 0, or bit 1 to some value, as otherwise, RAM will be visible in all three areas ($A000-$BFFF, $D000-$DFFF and $E000-$FFFF)
	sta $01
	
	ldy #0
-
	.for i=0, i<=3, i=i+1
		lda CHARSET_SOURCE_MEM+i*250,y
		sta CHARSET_DESTINATION_MEM+i*250,y
	.next
	iny
	cpy #250
	bne -

	; Restore memory configuration
	lda aMem
	sta $01
rts


screenLocationLo
	.for i=0, i<=24, i=i+1
		.byte <FIRST_SCREEN+40*i
	.next

screenLocationHi
	.for i=0, i<=24, i=i+1
		.byte >FIRST_SCREEN+40*i
	.next

secondScreenLocationLo
	.for i=0, i<=24, i=i+1
		.byte <SECOND_SCREEN+40*i
	.next

secondScreenLocationHi
	.for i=0, i<=24, i=i+1
		.byte >SECOND_SCREEN+40*i
	.next

.fi
