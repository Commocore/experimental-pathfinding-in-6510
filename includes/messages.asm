.enc screen

.if MANUAL_BRANCHES_RUN

nodeText .text "Node:  "

followBranchText .text "Follow branch: - (-)"

directionText
	.text " NE S   W"

.fi

.if MANUAL_PATHS_RUN

stepText .text "Step: @ "

pathText .text "Path: @ "

seekNodeText .text "Seek for branch with starting node: @ "

pathLoopText .text "Loop found. Path @ contains branch @."

registerBranchToPathText .text "Register branch @ to path @."

maxPathsOverflowText .text "Max paths overflow. Cloning aborted."

clonePathText .text "Clone path @ to @, register branch @"

sourceBranchText .text "Branch found: "

.fi

.if MANUAL_BRANCHES_RUN || MANUAL_PATHS_RUN || DRAW

listEmptyText .text "Empty"

listBranchesText .text "Branches:              "

listPathsText .text "Paths:"

listBranchesLengthText .text ", len: "

listMoreResultsText .text "Cursors UP/DOWN to see more results..."

cannotFindExitText .text "Cannot find exit. There's no hope."

finalText .text "Finished."

finalText2 .text "Shortest path found: @. Length: @."

finalText3 .text "Toggle SPACE bar to see result details."

.fi

.enc none
