
drawMap
	ldy map
	lda mapAddressLocationLo,y
	sta drawMapPointer+1
	lda mapAddressLocationHi,y
	sta drawMapPointer+2

	lda #174
	.for y=0, y<MAX_Y, y=y+1
		ldx #MAX_X-1
-
		sta FIRST_SCREEN + y*40 + MAP_SCREEN_OFFSET,x
		dex
		bpl -
	.next

	ldx #0
	ldy #0
	sty mapPointerOffset
-
	sty yMem
	ldy mapPointerOffset
drawMapPointer
	lda $1234,y
	ldy yMem
	jsr drawBlock
	ldy yMem
	
	inc mapPointerOffset
	inx
	cpx #MAX_X
	bne +
		ldx #0
		iny
		cpy #MAX_Y
		bne +
			rts
+
	jmp -


; input: X - x position
; input: Y - y position
; input: A - block number
drawBlock
	sta aMem
	stx xMem

	clc
	lda verticalColorRamTableLo,y
	adc xMem
	sta setColorRamBlock+1
	lda verticalColorRamTableHi,y
	adc #0
	sta setColorRamBlock+2

	cpx xStart
	bne +
		cpy yStart
		bne +
			lda #13
			jmp setColorRamBlock
+

	cpx xEnd
	bne +
		cpy yEnd
		bne +
			lda #2
			jmp setColorRamBlock
+
	
	ldy aMem
	lda blockColorTable,y
setColorRamBlock
	sta $1234
rts


drawNodeNumber
	clc
	ldy yNewNode
	lda verticalScreenTableLo,y
	adc xNewNode
	sta screenPosition+1
	lda verticalScreenTableHi,y
	adc #0
	sta screenPosition+2

	clc
	lda nodes
	cmp #10
	bcc +
		adc #65+128-11
		jmp screenPosition
+
	adc #48+128
screenPosition
	sta $1234
rts


blockColorTable
	.byte 11 ; non accessible
	.byte 12 ; track
	.byte 0 ; unused
	.byte 0 ; unused
	.byte 0 ; followed branch
	.byte 6 ; followed node
