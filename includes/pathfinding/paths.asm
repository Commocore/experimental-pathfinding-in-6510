
buildPathsFromBranches
	lda #0
	sta currentPath ; currently used path to build paths data
	sta registeredPaths
	sta registeredPathsStack
	sta currentStep ; current step indicates progress for each path (all paths builds in parallel so currentStep increases progressively)

	jsr registerInitialPaths
	jsr resetBranchesConnected


	; Iterate through all possible exits (paths which can go further) for a current step
iterateThroughAllPathsLoop

	ldy currentPath
	
	; Set paths direction pointers for current path
	lda pathsDirectionPointerLoTable,y
	sta currentPathsDirection

	lda pathsDirectionPointerHiTable,y
	sta currentPathsDirection+1
	
	; Set paths branches list pointers for current path
	lda pathsBranchPointerLoTable,y
	sta currentPathsBranch

	lda pathsBranchPointerHiTable,y
	sta currentPathsBranch+1
	
	ldy currentStep
	lda (currentPathsBranch),y ; get branch id
	beq advanceToNextPath

	sta yMem
	sta currentBranch

	ldy currentStep
	lda (currentPathsDirection),y ; if last direction is marked as opposite, set destination node as seekNode
	and #%10000000
	beq ++
		ldy yMem
		lda branchesSourceNodeTable,y
		cmp endNode
		bne +++
			; This path has end, and that's great, now advance to the next path
			jmp advanceToNextPath
+
		jmp ++
+
	ldy yMem
	lda branchesDestinationNodeTable,y ; get branch destination for this branch id
	cmp endNode
	bne +
		; This path has end, and that's great, now advance to the next path
		jmp advanceToNextPath
+
	sta seekNode

	ldy #0

	.if MANUAL_PATHS_RUN
		jsr displayPathStepStatusMessage
	.fi
	
destinationNodeLoop
	; Search for the specific node, which starts another branch so we can make path
	iny ; iterate through all branches, and as branches starts from 1, this loop starts from 1
	sty yMem
	
	cpy currentBranch ; skip if iterated branch is the same as the current one, as we're not going to go back within this branch
	beq +++
	
		; Check source node of branch
		lda branchesSourceNodeTable,y ; get source node of branch
		cmp seekNode ; and compare with node we're searching for
		bne +
			.if MANUAL_PATHS_RUN
				jsr displaySourceBranchMessage
				ldy yMem ; restore branch id
			.fi

			lda branchesStartingDirTable,y
			sta branchStartingDir

			jsr registerBranchToPath
			jmp ++ ; node found, branch registered, no need to check opposite end of branch below, so skip it
+
		; Check destination node of branch as it's possible that branch has been registered from opposite way while following a branch
		lda branchesDestinationNodeTable,y ; get destination node of branch
		cmp seekNode ; and compare with node we're searching for
		bne +
			.if MANUAL_PATHS_RUN
				jsr displaySourceBranchMessage
				ldy yMem ; restore branch id
			.fi

			lda branchesEndingDirTable,y
			ora #128 ; we're going also to transport extra information this way, that we're going from opposite direction of this branch, so destination and source are inverted
			sta branchStartingDir

			jsr registerBranchToPath
+

	ldy yMem
+
	cpy registeredBranches
	bne destinationNodeLoop

	; All branches has been iterated for the current path, advance to the next path to see if we can find any branches we can connect to the path
advanceToNextPath
	jsr cleanBranchesConnected

	inc currentPath
	ldy currentPath
	cpy registeredPaths
	beq +
		jmp iterateThroughAllPathsLoop
+

	lda anyBranchesConnected
	beq +
		; All paths has been iterated to connect branches to them, advance to the next step for all paths over again
		inc currentStep

		clc
		lda registeredPaths
		adc registeredPathsStack
		sta registeredPaths

		lda #0
		sta currentPath
		sta registeredPathsStack
		
		jsr resetBranchesConnected

		jmp iterateThroughAllPathsLoop
+

	; All paths built, now let's count length for paths which leads to the end point, and ignore these which don't lead
	ldx registeredPaths
-
		stx currentPath
		lda pathsBranchPointerLoTable,x
		sta word
		lda pathsBranchPointerHiTable,x
		sta word+1
		
		lda pathsStepTable,x ; get the last step for this path, as we want to check if it has finished for end point
		tay
		iny ; steps are starting from 0, but we're looking for length of path, so increment here
		lda (word),y ; get branch id
		tax
		lda branchesDestinationNodeTable,x ; TODO: Do we need to compare also source node as ends of each branch maybe should be treated the same? 
		; compare with end node of branch
		cmp endNode
		bne ++
			; this path leads to the end, so calculate length, otherwise ignore and advance to the next path
			sty currentStep

			clc
			lda #0
			sta aMem
-
			lda (word),y ; get branch id
			tax
			lda branchesLengthTable,x ; get length of this branch
			adc aMem
			sta aMem
			dey
			bpl -
+
		ldy currentPath
		sta pathsLengthTable,y
		cmp shortestPathLength
		bcs +
			sty shortestPath
			sta shortestPathLength
+
		ldx currentPath
		dex
		bpl --

	jsr buildOutputTrack ; Build output track format from the shortest path
	
	.if MANUAL_BRANCHES_RUN || MANUAL_PATHS_RUN || DRAW
		jsr displayFinalMessage
	.fi
	
	.if DRAW
		jsr drawShortestPath
	.fi
	
	.if MANUAL_BRANCHES_RUN || MANUAL_PATHS_RUN || DRAW
		jsr finalResultWait
	.fi
	
	.if DRAW
		jsr drawMap
	.fi

	; Cleaning path data at the very end, this way we can clean only there paths which has been filled
	ldx #0
-

	lda pathsBranchPointerLoTable,x
	sta cleanPathsBranchTablePointer+1
	lda pathsBranchPointerHiTable,x
	sta cleanPathsBranchTablePointer+2
	
	lda pathsDirectionPointerLoTable,x
	sta cleanPathsDirectionTablePointer+1
	lda pathsDirectionPointerHiTable,x
	sta cleanPathsDirectionTablePointer+2

	lda #0
	ldy #0
-
cleanPathsBranchTablePointer
		sta $1234,y
		
cleanPathsDirectionTablePointer
		sta $1234,y

		iny
		cpy #MAX_PATH_BRANCHES
		bcc -
	
	inx
	cpx registeredPaths
	bne --
rts


cleanBranchesConnected
	ldy #MAX_PATH_BRANCHES
	lda #0
-
	sta branchesConnected,y
	dey
	bpl -
rts


resetBranchesConnected
	lda #0
	sta anyBranchesConnected
	jmp cleanBranchesConnected
rts


registerInitialPaths
	ldx #1
-
	lda branchesSourceNodeTable,x
	cmp #1 ; starting node id is always = 1, try to find out all starting paths
	bne +
		ldy registeredPaths
		lda pathsBranchPointerLoTable,y
		sta initialPathsBranchTablePersist+1
		lda pathsBranchPointerHiTable,y
		sta initialPathsBranchTablePersist+2
		
		lda pathsDirectionPointerLoTable,y
		sta initialPathsDirectionTablePersist+1
		lda pathsDirectionPointerHiTable,y
		sta initialPathsDirectionTablePersist+2

		txa ; get branch id
initialPathsBranchTablePersist
		sta $1234

		lda branchesStartingDirTable,x
initialPathsDirectionTablePersist
		sta $1234
		
		inc registeredPaths
+
	inx
	cpx registeredBranches
	bcc -
	beq -
rts
	

; input: Y - branch id
registerBranchToPath
	sty aMem

	; Check if this branch is already registered to this path (this way we're also eliminating going back)
	ldy currentPath
	lda pathsStepTable,y
	tay
	iny
-
	lda (currentPathsBranch),y
	cmp aMem
	bne +
		.if MANUAL_PATHS_RUN
			jsr displayPathLoopMessage
		.fi
		rts ; this branch already exists in the path, we're not going to register it to prevent path loop (aka lasso)!
+
	dey
	bpl -
	
	; if there was already some branch connected for this particular node, it means that path is splitting,
	; so we need to clone the path and treat it as the separate one
	ldy seekNode
	lda branchesConnected,y
	beq +
		jmp clonePath
+

	.if MANUAL_PATHS_RUN
		jsr displayRegisterBranchToPathMessage
	.fi

	ldy aMem ; get branch id
	lda branchesLengthTable,y
	sta branchLength



.if DEBUG_MODE

	; DEBUG: Overwrite check test: If this will fail, something's wrong with paths branch table as this memory location should be empty!
	ldy currentStep
	iny
	lda (currentPathsBranch),y
	beq +
		.byte 2 ; JAM!
		.text "debug#2" ; small trick to see this string in monitor, so it's easier to find this place in code
+

.fi

	ldy currentStep
	iny ; branch for path is always one ahead as we're also including the starting branch id

	lda branchStartingDir
	sta (currentPathsDirection),y

	lda aMem
	sta (currentPathsBranch),y

	; Update how many steps this path contains
	lda currentStep
	ldy currentPath
	sta pathsStepTable,y

	; Branch has been connected from this node, so if there is another one, the path will be cloned and another branch will be registered there
	ldy seekNode
	clc
	lda branchesConnected,y
	adc #1
	sta branchesConnected,y
	inc anyBranchesConnected

rts


; As the path is splitting, clone the current path so far, and put the branch found on the end
; currentPath needs to be set to the path id to be copied
; aMem needs to be set to the current branch id as this branch is going to be registered
clonePath

	.if MANUAL_PATHS_RUN
		jsr displayClonePathMessage
	.fi

	ldy currentPath

	; Prepare pointers for source path nodes
	lda pathsBranchPointerLoTable,y
	sta sourcePathsNodePointers+1
	lda pathsBranchPointerHiTable,y
	sta sourcePathsNodePointers+2
	
	; Prepare pointers for source path directions
	lda pathsDirectionPointerLoTable,y
	sta sourceDirectionsPointers+1
	lda pathsDirectionPointerHiTable,y
	sta sourceDirectionsPointers+2

	; Adding new path (we need also to get registered paths from stack, which will be taken into account in the next step)
	clc
	lda registeredPaths
	adc registeredPathsStack
	tay
	
	cpy #MAX_PATHS
	bcc +
		; Allowed paths amount exceeded, returning...
		.if MANUAL_PATHS_RUN
			jsr displayMaxPathsOverflowMessage
		.fi
		rts
+

	; Update how many steps this new, cloned path contains
	lda currentStep
	sta pathsStepTable,y

	; Prepare pointers for destination path nodes
	lda pathsBranchPointerLoTable,y
	sta destinationPathsNodePointers+1
	lda pathsBranchPointerHiTable,y
	sta destinationPathsNodePointers+2

	; Prepare pointers for destination path directions
	lda pathsDirectionPointerLoTable,y
	sta destinationDirectionsPointers+1
	lda pathsDirectionPointerHiTable,y
	sta destinationDirectionsPointers+2

	; Clone path nodes
	ldy currentStep
	iny
	lda aMem ; get branch id
	jmp destinationPathsNodePointers ; small trick to clone whole path nodes but update the last branch id value with current one
-
sourcePathsNodePointers
		lda $1234,y

destinationPathsNodePointers
		sta $1234,y
		dey
	bpl -
	
	; Clone path directions
	ldy aMem ; get branch id
	lda branchesStartingDirTable,y ; TODO: watch out for the case when starting from opposite branch (ending), maybe it can occur?
	ldy currentStep
	iny
	jmp destinationDirectionsPointers ; small trick to clone whole path directions but update the last direction value with current one
-
sourceDirectionsPointers
		lda $1234,y

destinationDirectionsPointers
		sta $1234,y
		dey
	bpl -

	; Adding new path to stack, as we want to take it into account for the next step to iterate, but not for the current step
	inc registeredPathsStack

	inc anyBranchesConnected
	ldy seekNode
	clc
	lda branchesConnected,y
	adc #1
	sta branchesConnected,y
rts


; Build output track as we're going to translate all nodes to x, y coordinates instead
buildOutputTrack
	ldy shortestPath
	lda pathsBranchPointerLoTable,y
	sta readPathToTrack+1
	lda pathsBranchPointerHiTable,y
	sta readPathToTrack+2

	lda pathsDirectionPointerLoTable,y
	sta readDirectionToTrack+1
	lda pathsDirectionPointerHiTable,y
	sta readDirectionToTrack+2

	ldy #0
	ldx #0
-
	sty yMem ; shortest path step
readPathToTrack
	lda $1234,y ; get branch id
	bne +
		rts
+
	sta xMem

readDirectionToTrack
	lda $1234,y
	sta aMem
	and #%01111111
	sta outputTrackDir,x

	lda aMem
	ldy xMem ; get branch id
	and #%10000000
	beq +
		lda branchesSourceNodeTable,y
		jmp ++
	
+
	lda branchesDestinationNodeTable,y ; get node id
+
	tay
	lda nodesXTable,y ; get x position
	sta outputTrackX,x
	lda nodesYTable,y ; get y position
	sta outputTrackY,x

	inx
	ldy yMem
	iny
	jmp -
