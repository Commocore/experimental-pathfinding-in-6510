
mapVerticalPosition
	.for y=0, y<MAX_Y, y=y+1
		.byte y*MAX_X
	.next

; Opposite direction table used to follow the path and not going back, e.g. when going in direction = 2 (East), then the direction we came from is 8 (West)
fromDirectionTable
	.byte 0, 4, 8, 0, 1, 0, 0, 0, 2

.if DRAW

verticalColorRamTableLo
	.for y=0, y<MAX_Y, y=y+1
		.byte <$d800+y*40 + MAP_SCREEN_OFFSET
	.next

verticalColorRamTableHi
	.for y=0, y<MAX_Y, y=y+1
		.byte >$d800+y*40 + MAP_SCREEN_OFFSET
	.next

verticalScreenTableLo
	.for y=0, y<MAX_Y, y=y+1
		.byte <$400+y*40 + MAP_SCREEN_OFFSET
	.next

verticalScreenTableHi
	.for y=0, y<MAX_Y, y=y+1
		.byte >$400+y*40 + MAP_SCREEN_OFFSET
	.next

.fi

; table with node x coordinate results for each node
nodesXTable
	.for i=0, i<MAX_X * MAX_Y, i=i+1
		.byte 0
	.next

.cwarn * > nodesYTable, "FIXME: Tables overlapping!"

; table with node y coordinate results for each node
nodesYTable
	.for i=0, i<MAX_X * MAX_Y, i=i+1
		.byte 0
	.next

.cwarn * > nodesBranchTable, "FIXME: Tables overlapping!"

; table with branches result directions for each node
nodesBranchTable
	.for i=0, i<MAX_X * MAX_Y, i=i+1
		.byte 0
	.next

.cwarn * > nodesCurrentBranchTable, "FIXME: Tables overlapping!"

; table with branches direction results for each node, but value is changing when eliminating branches while finding the path
nodesCurrentBranchTable
	.for i=0, i<MAX_X * MAX_Y, i=i+1
		.byte 0
	.next

.cwarn * > branchesSourceNodeTable, "FIXME: Tables overlapping!"

; table with source node results for each branch (id is an index, so starts from 1)
.if DEBUG_TABLE_POINTER
* = $4000
.fi
branchesSourceNodeTable
	.for i=0, i<=MAX_BRANCHES, i=i+1
		.byte 0
	.next

; table with destination node results for each branch (id is an index, so starts from 1)
.if DEBUG_TABLE_POINTER
* = $4100
.fi
branchesDestinationNodeTable
	.for i=0, i<=MAX_BRANCHES, i=i+1
		.byte 0
	.next

; table with direction results that starts each branch (id is an index, so starts from 1)
.if DEBUG_TABLE_POINTER
* = $4200
.fi
branchesStartingDirTable
	.for i=0, i<=MAX_BRANCHES, i=i+1
		.byte 0
	.next

; table with direction results that ends each branch (id is an index, so starts from 1)
.if DEBUG_TABLE_POINTER
* = $4300
.fi
branchesEndingDirTable
	.for i=0, i<=MAX_BRANCHES, i=i+1
		.byte 0
	.next

; table with length results of each branch (id is an index, so starts from 1)
branchesLengthTable
	.for i=0, i<=MAX_BRANCHES, i=i+1
		.byte 0
	.next

.cwarn * > pathsBranchTable, "FIXME: Tables overlapping!"

.if DEBUG_TABLE_POINTER
* = $6000
.fi
pathsBranchTable
	.for i=0, i<MAX_PATHS, i=i+1
		.for x=0, x<MAX_PATH_BRANCHES, x=x+1 ; how many branches each path can have
			.byte 0
		.next
	.next

.cwarn * > pathsLengthTable, "FIXME: Tables overlapping!"

pathsLengthTable
	.for i=0, i<MAX_PATHS, i=i+1 ; results with length for indexed paths for each path
		.byte 0
	.next

.cwarn * > pathsStepTable, "FIXME: Tables overlapping!"

.if DEBUG_TABLE_POINTER
* = $7000
.fi
pathsStepTable
	.for i=0, i<MAX_PATHS, i=i+1 ; how many steps path contains
		.byte 0
	.next

.cwarn * > pathsDirectionTable, "FIXME: Tables overlapping!"
	
.if DEBUG_TABLE_POINTER
* = $8000
.fi
pathsDirectionTable
	.for i=0, i<MAX_PATHS, i=i+1
		.for x=0, x<MAX_PATH_BRANCHES, x=x+1 ; results with direction in what the branch follows for each path
			.byte 0
		.next
	.next

pathsBranchPointerLoTable
	.for i=0, i<MAX_PATHS, i=i+1
		.byte <pathsBranchTable + i * MAX_PATH_BRANCHES
	.next

pathsBranchPointerHiTable
	.for i=0, i<MAX_PATHS, i=i+1
		.byte >pathsBranchTable + i * MAX_PATH_BRANCHES
	.next

pathsDirectionPointerLoTable
	.for i=0, i<MAX_PATHS, i=i+1
		.byte <pathsDirectionTable + i * MAX_PATH_BRANCHES
	.next

pathsDirectionPointerHiTable
	.for i=0, i<MAX_PATHS, i=i+1
		.byte >pathsDirectionTable + i * MAX_PATH_BRANCHES
	.next

.cwarn * > branchesConnected, "FIXME: Tables overlapping!"

; keep information how many branches has been connected when constructing paths from branches
.if DEBUG_TABLE_POINTER
* = $8f00
.fi
branchesConnected
	.for x=0, x<MAX_PATH_BRANCHES, x=x+1
		.byte 0
	.next

.cwarn * > outputTrackX, "FIXME: Tables overlapping!"
	
; output track table with x position where direction needs to be changed when going with these directions
.if DEBUG_TABLE_POINTER
* = $9000
.fi
outputTrackX
	.for x=0, x<MAX_PATH_BRANCHES, x=x+1
		.byte 0
	.next

.cwarn * > outputTrackY, "FIXME: Tables overlapping!"
	
; output track table with x position where direction needs to be changed when going with these directions
.if DEBUG_TABLE_POINTER
* = $9100
.fi
outputTrackY
	.for x=0, x<MAX_PATH_BRANCHES, x=x+1
		.byte 0
	.next

.cwarn * > outputTrackDir, "FIXME: Tables overlapping!"
	
; output track table with direction to be changed
.if DEBUG_TABLE_POINTER
* = $9200
.fi
outputTrackDir
	.for x=0, x<MAX_PATH_BRANCHES, x=x+1
		.byte 0
	.next
