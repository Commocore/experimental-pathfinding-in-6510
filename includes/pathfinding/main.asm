
.if DEFAULT_VARIABLES_ADDRESSING

	aMem = $5
	xMem = $6
	yMem = $7

	xStart = $9
	yStart = $a
	xEnd = $b
	yEnd = $c
	nodes = $d
	branches = $e ; branches direction flags available for current node while building branches
	currentBranch = $e ; used to register current branch id when building paths

	xCurrentNode = $10
	word = $10 ; and $11
	yCurrentNode = $11

	xCurrentBranch = $12
	yCurrentBranch = $13
	
	currentPathsBranch = $12 ; and $13 - pointers used while creating paths from branches to record branch id

	xNewNode = $14
	yNewNode = $15
	currentPathsDirection = $14 ; and $15 - pointers used while creating paths from branches to record direction

	currentNode = $16 ; current node used while registering branches
	currentStep = $16 ; progress for each path while the final search for the shortest path
	
	branchesSeek = $17
	seekNode = $17

	ignoreDirection = $18
	currentPath = $18
	
	direction = $19
	branchStartingDir = $19
	
	directions = $1a
	branchLength = $1a

	currentBranchStartingDirection = $1b ; direction in which branch path starts to lead
	registeredBranches = $1c ; number of branches found and registered
	registeredBranchAddress = $1d ; and $1e
	currentBranchLength = $1f
	currentBranchEndingDirection = $20
	registeredPaths = $21
	registeredPathsStack = $22 ; if new path has been registered, update registered paths after loop for existing paths
	
	endNode = $23 ; keep end node id
	anyBranchesConnected = $24 ; indicator if any branches has been connected to any path in the step
	shortestPath = $25
	shortestPathLength = $26
	endNodeReached = $27 ; 1 if end node has been reached, otherwise we're not able to resolve paths from available branches anyway

.fi

.if FRAMES_COUNT
	framesCount = $30 ; for test purposes, counting frames
	rasterlineEnd = $31 ; for test purposes, counting ending rasterline
.fi


; Parameters needed to be set before calling the function:
; - pointers: mapPointer+1 (lo), mapPointer+2 (hi) to address of the map
; - starting point variables: xStart, yStart
; - end point variables: xEnd, yEnd
findPath
	.if RASTERLINE_INDICATION
		inc $d020
	.fi
	
	; Clean nodes coordinates
	jsr cleanNodesCoordinates

	lda #0
	sta nodes
	sta currentNode
	sta branchesSeek
	sta direction
	sta registeredBranches
	sta registeredPaths
	sta endNodeReached

	.if MANUAL_BRANCHES_RUN || MANUAL_PATHS_RUN
		jsr cleanMessages
	.fi
	
	.if MANUAL_BRANCHES_RUN
		jsr initBranchesDisplay
	.fi
	
	lda #$FF
	sta shortestPath
	sta shortestPathLength
	
	ldx xStart
	ldy yStart
	jsr createNode
	
findPathLoop
-
	inc currentNode
	ldy currentNode
	cpy nodes
	bcc +++
	beq +++
		lda branchesSeek ; does any branch have been sought, or all of them has been checked already?
		bne ++
			; Exploring all paths finished, time to find the shortest path
			
			.if RASTERLINE_INDICATION
				dec $d020
			.fi
			
			; Check if end node has been found at all, if not then finish here, otherwise advance to building paths from branches
			lda endNodeReached
			bne +
				.if MANUAL_BRANCHES_RUN || MANUAL_PATHS_RUN
					jsr displayCannotFindExitMessage
				.fi
				rts
+
			
			.if MANUAL_BRANCHES_RUN
				jsr cleanMessages
			.fi
			
			jsr buildPathsFromBranches

			rts
+
	lda #0
	sta branchesSeek
	lda #1
	sta currentNode
	ldy currentNode
+
	
	lda nodesCurrentBranchTable,y
	beq -
		inc branchesSeek
		
		; check all branches for this node
		jsr checkBranchesForCurrentNode
		jmp -


.include "nodes.asm"
.include "branches.asm"
.include "paths.asm"
.include "tables.asm"
