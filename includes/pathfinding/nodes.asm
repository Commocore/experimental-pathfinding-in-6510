
; input: X - current x position
; input: Y - current y position
; output: A - new node id
createNode
	stx xNewNode
	sty yNewNode

	jsr doesNodeExist
	beq +
		rts
+

	inc nodes
	ldy nodes

	txa
	sta nodesXTable,y

	lda yNewNode
	sta nodesYTable,y
	
	lda #0
	sta branches

	; Check block to the north
	ldy yNewNode
	beq +
		dey
		ldx xNewNode
		jsr getBlock
		beq +
			lda branches
			ora #1
			sta branches
+

	; Check block to the east
	ldx xNewNode
	cpx #MAX_X-1
	beq +
		inx
		ldy yNewNode
		jsr getBlock
		beq +
			lda branches
			ora #2
			sta branches
+

	; Check block to the south
	ldy yNewNode
	cpy #MAX_Y-1
	beq +
		iny
		ldx xNewNode
		jsr getBlock
		beq +
			lda branches
			ora #4
			sta branches
+

	; Check block to the west
	ldx xNewNode
	beq +
		dex
		ldy yNewNode
		jsr getBlock
		beq +
			lda branches
			ora #8
			sta branches
+

	; if we went from any direction, ignore (exclude) this direction for the node as we already know the distance
	ldy direction
	beq +
		sec
		lda branches
		sbc fromDirectionTable,y
		sta branches
+

	ldy nodes
	lda branches
	sta nodesBranchTable,y
	sta nodesCurrentBranchTable,y
	
	.if DRAW
		jsr drawNodeNumber
	.fi
	
	lda nodes
rts


; input: X - current x position
; input: Y - current y position
; output: A - node id
createEndNode
	stx xNewNode
	sty yNewNode

	jsr doesNodeExist
	beq +
		rts
+

	inc endNodeReached

	inc nodes
	ldy nodes
	
	sty endNode ; preserve the id here, will be used for the final part of finding the shortest path

	txa
	sta nodesXTable,y

	lda yNewNode
	sta nodesYTable,y

	lda #0
	sta nodesBranchTable,y
	sta nodesCurrentBranchTable,y
	
	.if DRAW
		jsr drawNodeNumber
	.fi
	
	lda nodes
rts


; output: A - boolean (1 = is a node, 0 = not a node)
isNode
	lda #0
	sta branches

	; Check block to the north
	ldy yCurrentBranch
	beq +
		dey
		ldx xCurrentBranch
		jsr getBlock
		beq +
			inc branches
+

	; Check block to the east
	ldx xCurrentBranch
	cpx #MAX_X-1
	beq +
		inx
		ldy yCurrentBranch
		jsr getBlock
		beq +
			inc branches
+

	; Check block to the south
	ldy yCurrentBranch
	cpy #MAX_Y-1
	beq +
		iny
		ldx xCurrentBranch
		jsr getBlock
		beq +
			inc branches
+

	; Check block to the west
	ldx xCurrentBranch
	beq +
		dex
		ldy yCurrentBranch
		jsr getBlock
		beq +
			inc branches
+

	lda branches
	cmp #3
	bcc +
		lda #1
		rts
+
	lda #0
rts


; xCurrentBranch and yCurrentBranch needs to be set to the current field
; output: A - return node id if exists, or return 0 if does not exist
doesNodeExist
	ldy nodes
	bne +
		lda #0 ; node not exists, actually there are no nodes yet
		rts
+

-
		lda xCurrentBranch
		cmp nodesXTable,y
		bne +
			lda yCurrentBranch
			cmp nodesYTable,y
			bne +
				; Node exists
				; As we went to the B node from A node, there is no need to check the branch in opposite direction from B to A, let's remove this direction flag then
				; This is the opposite branch exclusion (see: tests)
				ldx direction
				lda fromDirectionTable,x
				eor #255
				and nodesCurrentBranchTable,y
				sta nodesCurrentBranchTable,y
				tya ; return existing node id
				rts
+
	dey
	bne -

	lda #0 ; node not exists
rts


; input: x & y registers with x & y positions
; output: a - block number
getBlock
	txa
	clc
	adc mapVerticalPosition,y
	tay
mapPointer
	lda $1234,y
rts


cleanNodesCoordinates
	; Cleaning nodes coordinates at the very end of the stage with gaining all branches data
	; As we're cleaning data at the end, this way we can only clean data filled, cleaning the whole table would be slower
	ldy nodes
	lda #0
-
	sta nodesXTable,y
	sta nodesYTable,y
	dey
	bpl -
rts
