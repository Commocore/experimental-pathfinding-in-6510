
; input: A - direction from where we came (do not follow this one as then we would go back) - this is the direction which will be ignored in iteration
; input: X - initial direction in which the branch leads from the node
followBranch
	stx direction
	stx currentBranchStartingDirection
	stx currentBranchEndingDirection

	ldx xCurrentNode
	stx xCurrentBranch
	ldy yCurrentNode
	sty yCurrentBranch
	
	ldx #0
	stx currentBranchLength

; input: A - direction from where we came
followBranchExecute
	sta ignoreDirection
	lda direction
-
	cmp ignoreDirection
	beq followBranchNextDirection
		jmp followBranchInDirection

followBranchCheck
		inc currentBranchLength

		.if MANUAL_BRANCHES_RUN
			jsr displayDirectionMessage
		.fi
		
		; check if end position has been found
		ldx xCurrentBranch
		ldy yCurrentBranch
		cpx xEnd
		bne +
			cpy yEnd
			bne +
				jsr createEndNode
				jsr registerBranch
				rts
+

		jsr isNode
		beq followBranchNextStep
			;  node found, so end this branch
			
			ldx xCurrentBranch
			ldy yCurrentBranch
			jsr createNode
			jsr registerBranch
			
			.if DRAW
				; draw block for the node
				lda #5
				ldx xCurrentBranch
				ldy yCurrentBranch
				jsr drawBlock
			.fi
			
rts


followBranchNextStep

	.if DRAW
		; draw block for branch path
		lda #4
		ldx xCurrentBranch
		ldy yCurrentBranch
		jsr drawBlock
	.fi

	ldy direction
	lda fromDirectionTable,y
	
	ldx #1
	stx direction
	
	jmp followBranchExecute

followBranchNextDirection
	lda direction
	asl
	sta direction
	cmp #16
	bne -

	; all directions checked, move to next step of branch
rts


; input: A - direction
followBranchInDirection
	cmp #1
	bne +
		; Check if we can access block to the north
		ldy yCurrentBranch
		beq followBranchNextDirection
			dey
			ldx xCurrentBranch
			jsr getBlock
			beq followBranchNextDirection
				dec yCurrentBranch
				jmp followBranchCheck
+

	cmp #2
	bne +
		; Check if we can access block to the east
		ldx xCurrentBranch
		cpx #MAX_X-1
		beq followBranchNextDirection
			inx
			ldy yCurrentBranch
			jsr getBlock
			beq followBranchNextDirection
				inc xCurrentBranch
				jmp followBranchCheck
+

	cmp #4
	bne +
		; Check if we can access block to the south
		ldy yCurrentBranch
		cpy #MAX_Y-1
		beq followBranchNextDirection
			iny
			ldx xCurrentBranch
			jsr getBlock
			beq followBranchNextDirection
				inc yCurrentBranch
				jmp followBranchCheck
+

.if DEBUG_MODE

	; DEBUG: It shouldn't happen... never, as it should be the only last available option
	cmp #8
	beq +
		.byte 2 ; JAM!
		.text "debug#1" ; small trick to see this string in monitor, so it's easier to find this place in code
+

.fi

		; Check if we can access block to the west
		ldx xCurrentBranch
		beq followBranchNextDirection
			dex
			ldy yCurrentBranch
			jsr getBlock
			beq followBranchNextDirection
				dec xCurrentBranch
jmp followBranchCheck


; input: a - flag with all branches directions for this node (1 = north, 2 = east, 4 = south, 8 = west)
; input: y - current node
checkBranchesForCurrentNode
	sta directions
	
	.if MANUAL_BRANCHES_RUN
		jsr displayCurrentNodeMessage
	.fi
	
	lda nodesXTable,y
	sta xCurrentNode
	
	lda nodesYTable,y
	sta yCurrentNode

	lda #1
	bit directions
	beq +
		; check branch to the north
		;ldy currentNode ;TODO: This line is not needed for the very first time --- remove for optimization, if it will remain as it is now
		lda nodesCurrentBranchTable,y
		and #%11111110
		sta nodesCurrentBranchTable,y
		lda #4
		ldx #1
		jsr followBranch
+

	lda #2
	bit directions
	beq +
		; check branch to the east
		ldy currentNode
		lda nodesCurrentBranchTable,y
		and #%11111101
		sta nodesCurrentBranchTable,y
		lda #8
		ldx #2
		jsr followBranch
+

	lda #4
	bit directions
	beq +
		; check branch to the south
		ldy currentNode
		lda nodesCurrentBranchTable,y
		and #%11111011
		sta nodesCurrentBranchTable,y
		lda #1
		ldx #4
		jsr followBranch
+

	lda #8
	bit directions
	beq +
		; check branch to the west
		ldy currentNode
		lda nodesCurrentBranchTable,y
		and #%11110111
		sta nodesCurrentBranchTable,y
		lda #2
		ldx #8
		jsr followBranch
+
rts


; input: A - destination node id
registerBranch
	sta aMem
	
	; Record direction from which branch ends
	ldy direction
	lda fromDirectionTable,y
	sta currentBranchEndingDirection

	inc registeredBranches
	ldy registeredBranches

	lda currentNode
	sta branchesSourceNodeTable,y

	lda aMem
	sta branchesDestinationNodeTable,y

	lda currentBranchStartingDirection
	sta branchesStartingDirTable,y

	lda currentBranchEndingDirection
	sta branchesEndingDirTable,y

	lda currentBranchLength
	sta branchesLengthTable,y
rts
