
displayOppositeBranchExclusionFailMessage
	jsr unitTestCleanMessageField

	; Display test id
	lda dataIterator
	jsr extract8bitNumber

	ldy #0
	ldx #0
-
	lda unitTestOppositeBranchExclusionFailText,x
	bne +
		jsr unitTestDisplayNumber
+
	sta UNIT_TEST_MESSAGE_FIELD,y
	iny
	inx
	cpx #size(unitTestOppositeBranchExclusionFailText)
	bne -

	jsr unitTestDisplaySummary
rts


.enc screen

unitTestOppositeBranchExclusionFailText .text "test id @ failed: branch not equal."

.enc none
