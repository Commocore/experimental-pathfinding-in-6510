
displayEndNodeReachedFailMessage
	jsr unitTestCleanMessageField

	; Display test id
	lda dataIterator
	jsr extract8bitNumber

	ldy #0
	ldx #0
-
	lda unitTestEndNodeReachedFailText,x
	bne +
		jsr unitTestDisplayNumber
+
	sta UNIT_TEST_MESSAGE_FIELD,y
	iny
	inx
	cpx #size(unitTestEndNodeReachedFailText)
	bne -

	jsr unitTestDisplaySummary
rts


displayPathFailMessage
	jsr unitTestCleanMessageField

	; Display test id
	lda dataIterator
	jsr extract8bitNumber

	ldy #0
	ldx #0
-
	lda unitTestPathFailText,x
	bne +
		jsr unitTestDisplayNumber
+
	sta UNIT_TEST_MESSAGE_FIELD,y
	iny
	inx
	cpx #size(unitTestPathFailText)
	bne -

	jsr unitTestDisplaySummary
rts

displayLengthFailMessage
	jsr unitTestCleanMessageField

	; Display test id
	lda dataIterator
	jsr extract8bitNumber

	ldy #0
	ldx #0
-
	lda unitTestLengthFailText,x
	bne +
		jsr unitTestDisplayNumber
+
	sta UNIT_TEST_MESSAGE_FIELD,y
	iny
	inx
	cpx #size(unitTestLengthFailText)
	bne -

	jsr unitTestDisplaySummary
rts


.enc screen

unitTestEndNodeReachedFailText .text "map@ failed: is end point reached."

unitTestPathFailText .text "map@ failed: path not equal."

unitTestLengthFailText .text "map@ failed: length not equal."

.enc none
