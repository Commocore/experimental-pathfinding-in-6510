
UNIT_TEST_MESSAGE_FIELD = $400+23*40

unitTestCleanMessageField
	ldy #79
	lda #32
-
	sta UNIT_TEST_MESSAGE_FIELD,y
	dey
	bpl -
rts


unitTestDisplaySummary

	; Display expected value
	lda expected
	jsr extract8bitNumber

	ldy #40
	ldx #0
-
	lda unitTestExpectedText,x
	bne +
		jsr unitTestDisplayNumber
+
	sta UNIT_TEST_MESSAGE_FIELD,y
	iny
	inx
	cpx #size(unitTestExpectedText)
	bne -
	
	; Display actual value
	lda actual
	jsr extract8bitNumber

	ldx #0
-
	lda unitTestActualText,x
	bne +
		jsr unitTestDisplayNumber
+
	sta UNIT_TEST_MESSAGE_FIELD,y
	iny
	inx
	cpx #size(unitTestActualText)
	bne -
rts


unitTestDisplayNumber
	lda number100
	beq +
	adc #48
	sta UNIT_TEST_MESSAGE_FIELD,y
	iny
+
	lda number10
	beq +
	adc #48
	sta UNIT_TEST_MESSAGE_FIELD,y
	iny
+
	lda number1
	adc #48
rts


;input: A register
;output: affects number_1, number_10 and number_100
extract8bitNumber

	sta number

	lda #0
	sta number100
	sta number10
	sta number1

	lda number

	; hundreds
	sec
-
		cmp #100
		bcc +
		sbc #100
		inc number100
		jmp -
+

	; tens
	sec
-
		cmp #10
		bcc +
		sbc #10
		inc number10
		jmp -
+

	; ones
	clc
	sta number1

rts


; Assert failed, halt tests and show red border
unitTestAssertFailed
	; Change border colour to red
	lda #2
	sta $d020
	
	.if UNIT_TEST_EXIT_TO_BASIC
		jsr restoreBasicValues
		jmp unitTestExitToBasic
	.fi

jmp *


unitTestPassed
	; Change border colour to green
	lda #5
	sta $d020
	
	.if UNIT_TEST_EXIT_TO_BASIC
		jsr restoreBasicValues
		jmp unitTestExitToBasic
	.fi

jmp *


unitTestExitToBasic
	; Return to BASIC by resetting stack pointer
	ldx #$f6
	txs
rts


; Restore BASIC settings, otherwise it can crash if some parts of zero page has been used
restoreBasicValues
	; Default pointer to next expression in string stack, otherwise e.g. it can crash at $B4E0 for #$1
	lda #$19
	sta $16
rts


.enc screen

unitTestExpectedText .text "expected: @,"

unitTestActualText .text " actual: @."

.enc none
