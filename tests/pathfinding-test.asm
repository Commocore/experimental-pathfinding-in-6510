; Pathfinding case study test

	; Pathfinding algorithm settings

	DEFAULT_VARIABLES_ADDRESSING = 1 ; if not default, use your own addressing for all variables
	MANUAL_BRANCHES_RUN = 0	; run step by step for the first part for gaining branches data (fire button to advance)
	MANUAL_PATHS_RUN = 0 ; run step by step for the second part for building paths from branches (fire button to advance)
	DEBUG_MODE = 1 ; additional checkpoints for testing pitfall places
	DEBUG_TABLE_POINTER = 0 ; if set, some tables in includes/pathfinding/tables.asm file will be set with PC counter for debug purposes
	DRAW = 0 ; draw the path
	FRAMES_COUNT = 0 ; count how many frames the process will take, and get ending rasterline (counting from rasterline = 0), note that with DRAW it will be slower
	RASTERLINE_INDICATION = 0 ; show process length on border

	MAX_X = 13
	MAX_Y = 8
	MAX_BRANCHES = 47 ; how many branches can be created (each branch connects two nodes)
	MAX_PATHS = 47 ; how many paths can be created (each path can contain branches connected)
	MAX_PATH_BRANCHES = 48 ; how many branches path can contain
	MAX_MAPS = 13 ; how many maps to test

	; Unit test framework settings
	
	UNIT_TEST_EXIT_TO_BASIC = 1 ; Exit to BASIC when finished / assertion failed or do infinity loop?
	
	; If test will fail, here you will find the expected and actual values
	expected = $50
	actual = $51
	
	dataIterator = $52
	testPositionPointer = $53 ; and $54
	
	number = $55
	number1 = $56
	number10 = $57
	number100 = $58

*=$0801
	.word ss,10
	.null $9e,^start
ss	.word 0

start
	lda #147
	jsr $ffd2

	lda #0
	sta dataIterator
	sta expected
	sta actual

-
	ldy dataIterator
	cpy #MAX_MAPS+1
	bne +
		; end of tests, all green
		jmp unitTestPassed
+
	lda #46
	sta $400,y

	; Set map pointers for this particular map
	lda mapAddressLocationLo,y
	sta mapPointer+1
	lda mapAddressLocationHi,y
	sta mapPointer+2

	; Set start and end points for this particular map
	clc
	lda mapPointer+1
	adc #MAX_X * MAX_Y
	sta testPositionPointer
	lda mapPointer+2
	adc #0
	sta testPositionPointer+1

	ldy #0
	lda (testPositionPointer),y
	sta xStart

	iny
	lda (testPositionPointer),y
	sta yStart
	
	iny
	lda (testPositionPointer),y
	sta xEnd

	iny
	lda (testPositionPointer),y
	sta yEnd

	jsr findPath

	jsr assertEndNodeReached
	lda endNodeReached
	beq +
		jsr assertShortestPathEquals
		jsr assertLengthEquals
+

	inc dataIterator
	jmp -

; Include unit test messages
.include "includes/pathfinding-messages.asm"
.include "includes/core.asm"

; Include pathfinding functions needed to run the algorithm
.include "../includes/pathfinding/main.asm"
.include "../includes/maps.asm"


assertEndNodeReached
	lda endNodeReached
	ldy dataIterator
	cmp endNodeReachedExpected,y
	beq +
		sta actual
		
		lda endNodeReachedExpected,y
		sta expected
		
		jsr displayEndNodeReachedFailMessage
		jmp unitTestAssertFailed
+
rts


assertShortestPathEquals
	lda shortestPath
	ldy dataIterator
	cmp shortestPathExpected,y
	beq +
		sta actual
		
		lda shortestPathExpected,y
		sta expected

		jsr displayPathFailMessage

		jmp unitTestAssertFailed
+
rts


assertLengthEquals
	lda shortestPathLength
	ldy dataIterator
	cmp lengthExpected,y
	beq +
		sta actual
		
		lda lengthExpected,y
		sta expected

		jsr displayLengthFailMessage

		jmp unitTestAssertFailed
+
rts

; Expected values
endNodeReachedExpected
	.byte 1 ; map0
	.byte 1 ; map1
	.byte 1 ; map2
	.byte 1 ; map3
	.byte 1 ; map4
	.byte 1 ; map5
	.byte 1 ; map6
	.byte 1 ; map7
	.byte 1 ; map8
	.byte 1 ; map9
	.byte 1 ; map10
	.byte 1 ; map11
	.byte 0 ; map12
	.byte 1 ; map13

shortestPathExpected
	.byte 4 ;map0
	.byte 2 ;map1
	.byte 0 ;map2
	.byte 11 ;map3
	.byte 5 ;map4
	.byte 2 ;map5
	.byte 3 ;map6
	.byte 2 ;map7
	.byte 3 ;map8
	.byte 8 ;map9
	.byte 0 ;map10
	.byte 0 ;map11
	.byte 0 ;map12
	.byte 3 ;map13

lengthExpected
	.byte 19 ;map0
	.byte 19 ;map1
	.byte 21 ;map2
	.byte 15 ;map3
	.byte 14 ;map4
	.byte 13 ;map5
	.byte 13 ;map6
	.byte 13 ;map7
	.byte 13 ;map8
	.byte 9 ;map9
	.byte 15 ;map10
	.byte 15 ;map11
	.byte 0 ;map12
	.byte 17 ;map13

