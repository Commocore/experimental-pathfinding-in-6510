; Opposite branch exclusion test

	UNIT_TEST_EXIT_TO_BASIC = 1 ; Exit to BASIC when finished / assertion failed or do infinity loop?
	
	yMem = $5
	direction = $6

	expected = $a
	actual = $b
	dataIterator = $c

	number = $10
	number1 = $11
	number10 = $12
	number100 = $13
	
	; Variables needed for included files
	MAX_X = 0
	MAX_Y = 0
	DRAW = 0
	MAX_BRANCHES = 0
	MAX_PATHS = 0
	MAX_PATH_BRANCHES = 0
	DEBUG_TABLE_POINTER = 0
	
*=$0801
	.word ss,10
	.null $9e,^start
ss	.word 0

start

	lda #147
	jsr $ffd2

	lda #0
	sta dataIterator

-
	ldy dataIterator
	cpy #dataProviderDirectionEnd - dataProviderDirection
	bne +
		; end of tests, all green
		lda #5
		sta $d020
		rts
+
	lda dataProviderDirection,y
	sta direction

	lda #46
	sta $400,y

	lda dataProviderNodeBranches,y ; get flags of branches in this particular node
	
	ldy #9 ; node id, always keep the same as it doesn't matter

	sta nodesCurrentBranchTable,y
	jsr oppositeBranchExclusionFunction
	jsr assertEquals
	beq +
		; assert fails, halt tests and show red
		lda #2
		sta $d020
		rts
+

	inc dataIterator
	jmp -

; Includes
.include "../includes/pathfinding/tables.asm"

; Include unit test messages
.include "includes/opposite-branch-exclusion-messages.asm"
.include "includes/core.asm"

oppositeBranchExclusionFunction
	ldx direction
	lda fromDirectionTable,x
	eor #255
	and nodesCurrentBranchTable,y
	sta nodesCurrentBranchTable,y
	tya ; return existing node id

	rts

assertEquals
	lda nodesCurrentBranchTable,y
	ldy dataIterator
	cmp dataProviderExpected,y
	beq +
		sta actual
		
		lda dataProviderExpected,y
		sta expected

		jsr displayOppositeBranchExclusionFailMessage
		
		lda #1
		rts
+
	lda #0
rts

; Given there is a target node with these branches flags defined (1 = north, 2 = east, 4 = south, 8 = west)
dataProviderNodeBranches
	.byte 5, 5, 4, 10, 10, 8, 9, 15

; When node has been approached from <direction> direction
dataProviderDirection
	.byte 1, 4, 1, 2, 8, 2, 2, 8

dataProviderDirectionEnd
	 .byte 0
	
; Then node should remain with these flags so the direction from where we came to this target node should be excluded
dataProviderExpected
	.byte 1, 4, 0, 2, 8, 0, 1, 13
