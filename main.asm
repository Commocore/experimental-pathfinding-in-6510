
	DEFAULT_VARIABLES_ADDRESSING = 1 ; if not default, use your own addressing for all variables
	MANUAL_BRANCHES_RUN = 1	; run step by step for the first part for gaining branches data (fire button to advance)
	MANUAL_PATHS_RUN = 1 ; run step by step for the second part for building paths from branches (fire button to advance)
	DEBUG_MODE = 1 ; additional checkpoints for testing pitfall places
	DEBUG_TABLE_POINTER = 0 ; if set, some tables in includes/pathfinding/tables.asm file will be set with PC counter for debug purposes
	DRAW = 1 ; draw the path
	FRAMES_COUNT = 1 ; count how many frames the process will take, and get ending rasterline (counting from rasterline = 0), note that with DRAW it will be slower
	RASTERLINE_INDICATION = 1 ; show process length on border
	
	MAX_X = 13
	MAX_Y = 8
	MAX_BRANCHES = 47 ; how many branches can be created (each branch connects two nodes)
	MAX_PATHS = 47 ; how many paths can be created (each path can contain branches connected)
	MAX_PATH_BRANCHES = 48 ; how many branches path can contain (how many steps)

	MAX_MAPS = 15 ; number of maps, see includes/maps.asm for all available map setups, or create your own!

	.if DRAW || MANUAL_BRANCHES_RUN || MANUAL_PATHS_RUN
		FIRST_SCREEN = $400
		FIRST_SCREEN_MEMORY = #%00010000
		FIRST_SCREEN_CHARSET = #%00000110
		SECOND_SCREEN = $3000 + $c000
		SECOND_SCREEN_MEMORY = #%11000000
		SECOND_SCREEN_CHARSET = #%00000000
		CHARSET_SOURCE_MEM = $d800 ; source location of standard ROM charset
		CHARSET_DESTINATION_MEM = $c000 ; destination location to copy a standard charset, to bank #3 under $C000
		COLOR_RAM = $d800
		COLOR_RAM_STORAGE = $5000 ; memory location used for storing Color RAM data when switching between screens
		MAP_SCREEN_OFFSET = 40*8 + 13

		number = $50
		number1 = $51
		number10 = $52
		number100 = $53
		
		screen = $54 ; and $55
		
		var1 = $56
		var2 = $57
		var3 = $58
		var4 = $59
		var5 = $5a

		toggleScreenCtrl = $5b
		nextStepCtrl = $5c
		nextStepCtrlFireButton = $5d
		listScreenScrollCtrl = $5e
		
		listScreenOffset = $5f
	.fi

	mapPointerOffset = $8
	findPathProcess = $f
	map = $40

*=$0801
	.word ss,10
	.null $9e,^start
ss	.word 0

start

	lda #0 ; select map (starts from 0)
	sta map

	jsr init
	jsr initMap
	
-
	; find path process will run outside of IRQ (but triggered from IRQ at the top of the screen in rasterline = 0), as it tooks more than 1 frame to process
	lda findPathProcess
	beq +
		cmp #1
		bne +

			inc findPathProcess

			lda #0
			sta listScreenOffset

			jsr findPath
			
			lda #0
			sta findPathProcess
			
			.if FRAMES_COUNT
				lda $d012
				sta rasterlineEnd
			.fi
+
	jmp -

	
initMap
	ldy map
	lda mapAddressLocationLo,y
	sta mapPointer+1
	sta word
	lda mapAddressLocationHi,y
	sta mapPointer+2
	sta word+1

	ldy #MAX_X * MAX_Y
	lda (word),y
	sta xStart
	
	iny
	lda (word),y
	sta yStart
	
	iny
	lda (word),y
	sta xEnd
	
	iny
	lda (word),y
	sta yEnd
	
	.if DRAW
		jsr drawMap
	.fi
rts

.include "includes/init.asm"
.include "includes/irq.asm"
.if DRAW
	.include "includes/drawing.asm"
	.include "includes/drawing-shortest-path.asm"
.fi
.include "includes/maps.asm"
.if MANUAL_BRANCHES_RUN || MANUAL_PATHS_RUN || DRAW
	.include "includes/manual-run-messages.asm"
	.include "includes/messages.asm"
.fi
.include "includes/pathfinding/main.asm"
